package com.springbootangular.demo.controller;

import com.springbootangular.demo.exception.ResourceNotFoundException;
import com.springbootangular.demo.model.Todo;
import com.springbootangular.demo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TodoController {
    @Autowired
    private TodoRepository todoRepository;

    @GetMapping("/todos")
    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<Todo> getTodoById(@PathVariable(value = "id") Long id)
        throws ResourceNotFoundException {
            Todo todo = todoRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found."));
            return ResponseEntity.ok().body(todo);
        }
    @PostMapping("/todos")
    public Todo createTodo(@Valid @RequestBody Todo todo) {
        return todoRepository.save(todo);
    }

    @PutMapping("/todos/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable(value="id") Long id,
        @Valid @RequestBody Todo todos) throws ResourceNotFoundException {
            Todo todo = todoRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

        todo.setName(todos.getName());
        todo.setDescription(todos.getDescription());
        todo.setStatus(todos.getStatus());
        todo.setUser_id(todos.getUser_id());
            final Todo updatedTodo = todoRepository.save(todo);
            return ResponseEntity.ok(updatedTodo);
    }

    @DeleteMapping("/todos/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        Todo todo = todoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + id));

        todoRepository.delete(todo);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
